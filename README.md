### GYM WORKOUT BACKEND

#### Devs

- Cleber Silva
- Julio Bottesini

#### Running Application

- H2 
    1. ./gradlew bootRun -Dspring.profiles.active=local
    2. verify created DB at: http://localhost:8080/gym-service/h2-console
- MYSQL
    1. docker run -p 3306:3306 -e MYSQL_ALLOW_EMPTY_PASSWORD=yes -e MYSQL_DATABASE=gym-orch -d mysql:8
    2. ./gradlew bootRun -Dspring.profiles.active=qa

Test: http://localhost:8080/gym-service