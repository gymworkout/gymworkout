package br.com.gymworkout;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GymWorkoutApplication {

	public static void main(String[] args) {
		SpringApplication.run(GymWorkoutApplication.class, args);
	}
}
