package br.com.gymworkout.converters.dto;

import br.com.gymworkout.dtos.AdminDto;
import br.com.gymworkout.dtos.MemberDto;
import br.com.gymworkout.dtos.PersonDto;
import br.com.gymworkout.dtos.TrainerDto;
import br.com.gymworkout.model.Admin;
import br.com.gymworkout.model.Member;
import br.com.gymworkout.model.Person;
import br.com.gymworkout.model.Trainer;

public class PersonDtoConverter implements PersonDtoVisitor {

	@Override
	public PersonDto visit(Admin admin) {
		AdminDto adminDto = new AdminDto();

		setSuperClassProperties(admin, adminDto);

		return adminDto;
	}

	@Override
	public PersonDto visit(Member member) {
		MemberDto memberDto = new MemberDto();

		setSuperClassProperties(member, memberDto);

		return memberDto;
	}

	@Override
	public PersonDto visit(Trainer trainer) {
		TrainerDto trainerDto = new TrainerDto();

		setSuperClassProperties(trainer, trainerDto);

		trainerDto.setTest(trainer.getTest());

		return trainerDto;
	}

	private void setSuperClassProperties(Person person, PersonDto personDto) {
		personDto.setId(person.getId());
		personDto.setName(person.getName());
		personDto.setEnabled(person.isEnabled());
		personDto.setTenantId(person.getTenantId());
		personDto.setType(person.getType());
		personDto.setCpf(person.getCpf());
		personDto.setPassword("***");
	}
}
