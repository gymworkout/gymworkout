package br.com.gymworkout.converters.dto;


import br.com.gymworkout.dtos.PersonDto;
import br.com.gymworkout.model.Admin;
import br.com.gymworkout.model.Member;
import br.com.gymworkout.model.Trainer;

public interface PersonDtoVisitor {
	PersonDto visit(Admin admin);

	PersonDto visit(Member member);

	PersonDto visit(Trainer trainer);
}