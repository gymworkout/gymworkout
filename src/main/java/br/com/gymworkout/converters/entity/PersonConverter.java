package br.com.gymworkout.converters.entity;

import br.com.gymworkout.config.tenant.TenantContext;
import br.com.gymworkout.dtos.AdminDto;
import br.com.gymworkout.dtos.MemberDto;
import br.com.gymworkout.dtos.PersonDto;
import br.com.gymworkout.dtos.TrainerDto;
import br.com.gymworkout.model.Admin;
import br.com.gymworkout.model.Member;
import br.com.gymworkout.model.Person;
import br.com.gymworkout.model.Trainer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static br.com.gymworkout.model.Authority.ROLE_MEMBER;

public class PersonConverter implements PersonVisitor {

	@Override
	public Person visit(AdminDto adminDto) {
		Admin admin = new Admin();

		setSuperClassProperties(admin, adminDto);

		return admin;
	}

	@Override
	public Person visit(MemberDto memberDto) {
		Member member = new Member();

		setSuperClassProperties(member, memberDto);

		return member;
	}

	@Override
	public Person visit(TrainerDto trainerDto) {
		Trainer trainer = new Trainer();

		setSuperClassProperties(trainer, trainerDto);

		trainer.setTest(trainerDto.getTest());

		return trainer;
	}

	private void setSuperClassProperties(Person person, PersonDto personDto) {
		person.setId(personDto.getId());
		person.setName(personDto.getName());
		person.setEnabled(true);
		person.setTenantId(TenantContext.getCurrentTenantEnum());
		person.setType(personDto.getType());
		person.setCpf(personDto.getCpf());
		person.setAuthority(ROLE_MEMBER);
		person.setPassword(new BCryptPasswordEncoder().encode(personDto.getPassword()));
	}
}
