package br.com.gymworkout.converters.entity;

import br.com.gymworkout.dtos.AdminDto;
import br.com.gymworkout.dtos.MemberDto;
import br.com.gymworkout.dtos.TrainerDto;
import br.com.gymworkout.model.Person;

public interface PersonVisitor {
	Person visit(AdminDto admin);

	Person visit(MemberDto memberDto);

	Person visit(TrainerDto trainerDto);
}