package br.com.gymworkout.endpoints;

import br.com.gymworkout.dtos.PersonDto;
import br.com.gymworkout.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.net.URI;
import java.util.Objects;

import static br.com.gymworkout.infra.Constants.API_PATH;
import static br.com.gymworkout.infra.Constants.PERSON;


@RestController
@RequestMapping(PERSON)
public class PersonEndpoint {

	@Autowired
	private PersonService personService;

	@RolesAllowed("ROLE_ADMIN")
	@PostMapping
	public ResponseEntity save(@RequestBody @Valid PersonDto personDto) {

		personDto = personService.create(personDto);

		URI location = URI.create(UriComponentsBuilder
				.fromPath(API_PATH.concat(PERSON))
				.pathSegment(Objects.toString(personDto.getId()))
				.toUriString());

		return ResponseEntity.created(location).build();
	}

	@RolesAllowed({"ROLE_MEMBER", "ROLE_ADMIN"})
	@GetMapping(value = "{personId}")
	public ResponseEntity get(@PathVariable("personId") Long personId) {

		PersonDto personDto = personService.get(personId);

		return ResponseEntity.ok(personDto);
	}

	@RolesAllowed({"ROLE_MEMBER", "ROLE_ADMIN"})
	@PutMapping(value = "{personId}")
	public ResponseEntity update(@PathVariable("personId") Long personId,@RequestBody @Valid PersonDto personDto) {

		personDto = personService.update(personDto, personId);

		return ResponseEntity.ok(personDto);
	}
}
