package br.com.gymworkout.model;

import br.com.gymworkout.config.tenant.TenantId;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "USER", schema = "default")
@Data
public class User implements UserDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "CPF", unique = true)
	private String cpf;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "AUTHORITY")
	@Enumerated(EnumType.STRING)
	private Authority authority;

	@Column(name = "TENANT")
	@Enumerated(EnumType.STRING)
	private TenantId tenantId;

	@Column(name = "ENABLED")
	private boolean enabled;

	@Column(name = "TYPE")
	@Enumerated(EnumType.STRING)
	private PersonType type;

	@Override
	public String getUsername() {
		return cpf;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Arrays.asList(new SimpleGrantedAuthority(Objects.toString(authority)));
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
}
