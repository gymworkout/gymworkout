package br.com.gymworkout.model;

public enum PersonType {
	MEMBER, TRAINER, ADMIN, PERSON
}
