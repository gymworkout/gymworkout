package br.com.gymworkout.model;

import br.com.gymworkout.converters.dto.PersonDtoVisitor;
import br.com.gymworkout.dtos.PersonDto;

import javax.persistence.Entity;

@Entity
public class Admin extends Person {

	@Override
	public PersonDto convert(PersonDtoVisitor personDtoVisitor) {
		return personDtoVisitor.visit(this);
	}
}
