package br.com.gymworkout.model;

public enum Authority {
	ROLE_ADMIN, ROLE_TRAINER, ROLE_MEMBER
}
