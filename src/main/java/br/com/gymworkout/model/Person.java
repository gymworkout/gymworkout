package br.com.gymworkout.model;

import br.com.gymworkout.config.PersonListener;
import br.com.gymworkout.config.tenant.TenantId;
import br.com.gymworkout.converters.dto.PersonDtoVisitor;
import br.com.gymworkout.dtos.PersonDto;
import br.com.gymworkout.infra.Visitable;
import lombok.Data;

import javax.persistence.*;

@Table(name = "PERSON")
@Data
@Entity
@EntityListeners(PersonListener.class)
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Person implements Visitable<PersonDto, PersonDtoVisitor> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "CPF")
	private String cpf;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "AUTHORITY")
	@Enumerated(EnumType.STRING)
	private Authority authority;

	@Column(name = "TENANT")
	@Enumerated(EnumType.STRING)
	private TenantId tenantId;

	@Column(name = "ENABLED")
	private boolean enabled;

	@Column(name = "TYPE")
	@Enumerated(EnumType.STRING)
	private PersonType type;

	@Column(name = "NAME")
	private String name;
}

