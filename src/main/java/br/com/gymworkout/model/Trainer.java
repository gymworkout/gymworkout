package br.com.gymworkout.model;

import br.com.gymworkout.converters.dto.PersonDtoVisitor;
import br.com.gymworkout.dtos.PersonDto;
import lombok.Data;

import javax.persistence.Entity;

@Entity
@Data
public class Trainer extends Person {

	private String test;

	@Override
	public PersonDto convert(PersonDtoVisitor personDtoVisitor) {
		return personDtoVisitor.visit(this);
	}
}
