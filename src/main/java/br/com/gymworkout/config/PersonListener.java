package br.com.gymworkout.config;

import br.com.gymworkout.model.Person;
import br.com.gymworkout.model.User;
import br.com.gymworkout.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;


@Component
public class PersonListener {

	@Autowired
	private ApplicationContext ctx;

	@PostPersist
	void onPostPersist(Person person) {
		UserRepository userRepository = ctx.getBean(UserRepository.class);
		User user = buildUser(person);
		userRepository.save(user);
	}

	@PostUpdate
	void onPostUpdate(Person person) {
		UserRepository userRepository = ctx.getBean(UserRepository.class);
		User user = buildUser(person);
		user.setId(person.getId());
		userRepository.save(user);
	}

	private User buildUser(Person person) {
		User user = new User();
		user.setCpf(person.getCpf());
		user.setPassword(person.getPassword());
		user.setTenantId(person.getTenantId());
		user.setEnabled(person.isEnabled());
		user.setAuthority(person.getAuthority());
		user.setType(person.getType());
		return user;
	}
}
