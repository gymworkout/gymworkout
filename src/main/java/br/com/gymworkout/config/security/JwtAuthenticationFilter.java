package br.com.gymworkout.config.security;

import br.com.gymworkout.model.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URI;
import java.util.Date;
import java.util.Objects;
import java.util.stream.Collectors;

import static br.com.gymworkout.config.security.SecurityConstants.TOKEN_HEADER;
import static br.com.gymworkout.infra.Constants.*;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private final AuthenticationManager authenticationManager;

	public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;

		setFilterProcessesUrl(SecurityConstants.AUTH_LOGIN_URL);
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {

		var username = request.getParameter("cpf");
		var password = request.getParameter("password");
		var authenticationToken = new UsernamePasswordAuthenticationToken(username, password);

		return authenticationManager.authenticate(authenticationToken);
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
											FilterChain filterChain, Authentication authentication) {
		var user = ((User) authentication.getPrincipal());

		var roles = user.getAuthorities()
				.stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.toList());

		var signingKey = SecurityConstants.JWT_SECRET.getBytes();

		var token = Jwts.builder()
				.signWith(Keys.hmacShaKeyFor(signingKey), SignatureAlgorithm.HS512)
				.setHeaderParam("typ", SecurityConstants.TOKEN_TYPE)
				.setIssuer(SecurityConstants.TOKEN_ISSUER)
				.setAudience(SecurityConstants.TOKEN_AUDIENCE)
				.setSubject(user.getUsername())
				.setExpiration(new Date(System.currentTimeMillis() + 864000000))
				.claim("rol", roles)
				.compact();

		response.addHeader(TOKEN_HEADER, SecurityConstants.TOKEN_PREFIX + token);
		response.addHeader(CPF, user.getCpf());
		response.addHeader(X_TENANT_ID, user.getTenantId().name());
		response.addHeader(USER_ID, user.getId().toString());
		response.addHeader(LOCATION, Objects.toString(URI.create(UriComponentsBuilder
												.fromPath(API_PATH.concat(PERSON))
												.pathSegment(Objects.toString(user.getId())).toUriString())));
	}
}