package br.com.gymworkout.config.security;

import io.jsonwebtoken.Jwts;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static br.com.gymworkout.config.security.SecurityConstants.*;

@Log4j2
public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

	public JwtAuthorizationFilter(AuthenticationManager authenticationManager) {
		super(authenticationManager);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {

		var header = request.getHeader(TOKEN_HEADER);

		if (StringUtils.startsWith(header, TOKEN_PREFIX)) {
			SecurityContextHolder.getContext().setAuthentication(getAuthentication(request));
		}

		filterChain.doFilter(request, response);
	}

	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {

		var token = request.getHeader(TOKEN_HEADER);

		try {
			var signingKey = JWT_SECRET.getBytes();

			var parsedToken = Jwts.parser()
					.setSigningKey(signingKey)
					.parseClaimsJws(token.replace("Bearer ", ""));

			var username = parsedToken
					.getBody()
					.getSubject();

			var authorities = ((List<?>) parsedToken.getBody()
					.get("rol")).stream()
					.map(authority -> new SimpleGrantedAuthority((String) authority))
					.collect(Collectors.toList());

			if (StringUtils.isNotEmpty(username)) {
				return new UsernamePasswordAuthenticationToken(username, null, authorities);
			}
		} catch (Exception exception) {
			log.warn(exception.getMessage() + " token=" + token, exception);
		}
		return null;
	}
}
