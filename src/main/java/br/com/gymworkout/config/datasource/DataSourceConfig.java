package br.com.gymworkout.config.datasource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

@Configuration
public class DataSourceConfig {

	@Value("${file.location}")
	private String file;

	@Autowired
	private Properties props;

	@Bean
	@Profile("dev")
	@DependsOn({"lookup"})
	public DataSource dev() throws IOException {
		return getDataSource();
	}

	@Bean
	@Profile("prod")
	@DependsOn({"lookup"})
	public DataSource prod() throws IOException {
		return getDataSource();
	}

	private DataSource getDataSource() throws IOException {
		props.load(getClass().getResourceAsStream(file));
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUrl(Objects.toString(props.get("hibernate.connection.url")));
		dataSource.setUsername(Objects.toString(props.get("hibernate.connection.username")));
		dataSource.setConnectionProperties(props);
		return dataSource;
	}
}
