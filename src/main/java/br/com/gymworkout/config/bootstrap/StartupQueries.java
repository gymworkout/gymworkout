package br.com.gymworkout.config.bootstrap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;

public enum StartupQueries {

	CREATE_SCHEMA("CREATE SCHEMA IF NOT EXISTS {0};"),
	SET_SCHEMA("SET SCHEMA {0};"),

	TABLE_PERSON("CREATE TABLE IF NOT EXISTS {0}.PERSON " +
			"(id identity, name varchar(256), cpf varchar(256) unique, password varchar(256), " +
			"enabled boolean, authority varchar(256), tenant varchar(256), type varchar(256));"),

	TABLE_MEMBER("CREATE TABLE IF NOT EXISTS {0}.MEMBER(id identity);"),
	TABLE_ADMIN("CREATE TABLE IF NOT EXISTS {0}.ADMIN (id identity);"),
	TABLE_TRAINER("CREATE TABLE IF NOT EXISTS {0}.TRAINER (id identity, test varchar(256));");


	private String query;

	StartupQueries(String query) {
		this.query = query;
	}

	public String getQuery() {
		return query;
	}

	static final List<String> QUERIES;

	static {
		List<String> temp = new ArrayList<>();
		asList(StartupQueries.values()).forEach(startupQuery -> temp.add(startupQuery.getQuery()));
		QUERIES = Collections.unmodifiableList(temp);
	}
}
