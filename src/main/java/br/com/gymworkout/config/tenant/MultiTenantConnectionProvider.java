package br.com.gymworkout.config.tenant;

import br.com.gymworkout.infra.BeansLookupWrapper;
import org.hibernate.engine.jdbc.connections.internal.DriverManagerConnectionProviderImpl;
import org.hibernate.engine.jdbc.connections.spi.AbstractMultiTenantConnectionProvider;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.service.spi.Startable;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class MultiTenantConnectionProvider extends AbstractMultiTenantConnectionProvider implements Startable {

	private DriverManagerConnectionProviderImpl connectionProvider;
	private BeansLookupWrapper beansLookup;

	public MultiTenantConnectionProvider() {
		beansLookup = new BeansLookupWrapper();
		connectionProvider = new DriverManagerConnectionProviderImpl();
	}

	@Override
	public void start() {
		Properties props = beansLookup.getBean(DriverManagerDataSource.class).getConnectionProperties();
		connectionProvider.configure(props);
	}

	@Override
	protected ConnectionProvider getAnyConnectionProvider() {
		return connectionProvider;
	}

	@Override
	protected ConnectionProvider selectConnectionProvider(String tenantIdentifier) {
		return connectionProvider;
	}

	@Override
	public Connection getConnection(String tenantIdentifier) throws SQLException {
		Properties props = beansLookup.getBean(DriverManagerDataSource.class).getConnectionProperties();
		Connection connection = super.getConnection(tenantIdentifier);
		Statement stmt = connection.createStatement();
		stmt.execute(props.getProperty("set.schema.sintax") + tenantIdentifier);
		return connection;
	}
}
