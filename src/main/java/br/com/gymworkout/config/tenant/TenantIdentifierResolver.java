package br.com.gymworkout.config.tenant;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.util.StringUtils;

public class TenantIdentifierResolver implements CurrentTenantIdentifierResolver {

	private static final String DEFAULT_TENANT = "default";

	@Override
	public String resolveCurrentTenantIdentifier() {
		String tenant = TenantContext.getCurrentTenant();
		return StringUtils.isEmpty(tenant) ? DEFAULT_TENANT : tenant;
	}

	@Override
	public boolean validateExistingCurrentSessions() {
		return true;
	}
}
