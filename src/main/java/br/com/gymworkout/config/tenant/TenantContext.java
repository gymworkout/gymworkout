package br.com.gymworkout.config.tenant;

import static br.com.gymworkout.config.tenant.TenantId.TENANTS;

public class TenantContext {

	private TenantContext() {
	}

	private static final ThreadLocal<String> CURRENT_TENANT = new ThreadLocal<>();

	public static void setCurrentTenant(String tenant) {
		CURRENT_TENANT.set(tenant);
	}

	public static String getCurrentTenant() {
		return CURRENT_TENANT.get();
	}

	public static TenantId getCurrentTenantEnum() {
		return TENANTS.get(TenantContext.getCurrentTenant());
	}

	public static void clear() {
		CURRENT_TENANT.set(null);
	}
}
