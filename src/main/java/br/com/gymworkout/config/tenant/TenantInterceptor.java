package br.com.gymworkout.config.tenant;

import br.com.gymworkout.model.User;
import br.com.gymworkout.repositories.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

import static br.com.gymworkout.infra.Constants.X_TENANT_ID;
import static java.util.Arrays.stream;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Component
public class TenantInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	private UserRepository repository;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
		String tenantId = request.getHeader(X_TENANT_ID);
		String cpf = request.getHeader("cpf");

		tenantId = isNotEmpty(tenantId) ? tenantId.toUpperCase() : discoveryUser(cpf).getTenantId().name().toUpperCase();

		List<String> tenantIds = stream(TenantId.values()).map(TenantId::name).collect(Collectors.toList());

		if (tenantIds.contains(tenantId)) {
			TenantContext.setCurrentTenant(tenantId);
			return true;
		}

		throw throwIllegalCaller();
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
		TenantContext.clear();
	}

	private User discoveryUser(String cpf) {
		return repository.findByCpf(cpf).orElseThrow(new TenantInterceptor()::throwIllegalCaller);
	}

	private RuntimeException throwIllegalCaller() {
		return new IllegalCallerException("could not determine current tenantId");
	}
}
