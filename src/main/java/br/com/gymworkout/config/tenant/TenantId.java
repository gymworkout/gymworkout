package br.com.gymworkout.config.tenant;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum TenantId {
	LIV, HABITUS, MAXGYM;

	public static final Map<String, TenantId> TENANTS;

	static {
		Map<String, TenantId> temp = new HashMap<>();
		Arrays.asList(TenantId.values()).stream().forEach(tenantId ->  temp.put(tenantId.name(), tenantId));
		TENANTS = Collections.unmodifiableMap(temp);
	}
}
