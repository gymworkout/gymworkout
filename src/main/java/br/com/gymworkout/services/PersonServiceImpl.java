package br.com.gymworkout.services;

import br.com.gymworkout.converters.dto.PersonDtoConverter;
import br.com.gymworkout.converters.entity.PersonConverter;
import br.com.gymworkout.dtos.PersonDto;
import br.com.gymworkout.model.Person;
import br.com.gymworkout.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {

	private static final String NOT_FOUND_MSG = "person not found id=";

	@Autowired
	private PersonRepository personRepository;

	@Override
	public PersonDto create(PersonDto personDto) {
		Person converted = personDto.convert(new PersonConverter());

		Person person = personRepository.save(converted);

		return person.convert(new PersonDtoConverter());
	}

	@Override
	public PersonDto get(Long personId) {
		Person person = personRepository.findById(personId)
				.orElseThrow(() -> new EntityNotFoundException(NOT_FOUND_MSG + personId));

		return person.convert(new PersonDtoConverter());
	}

	@Override
	public PersonDto update(PersonDto personDto, Long personId) {
		if (personRepository.existsById(personId)) {
			personDto.setId(personId);
			Person person = personRepository.save(personDto.convert(new PersonConverter()));
			return  person.convert(new PersonDtoConverter());
		}
		throw new EntityNotFoundException(NOT_FOUND_MSG + personId);
	}
}
