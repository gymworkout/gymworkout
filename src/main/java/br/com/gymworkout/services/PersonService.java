package br.com.gymworkout.services;

import br.com.gymworkout.dtos.PersonDto;

public interface PersonService {

	PersonDto get(Long personId);

	PersonDto create(PersonDto personDto);

	PersonDto update(PersonDto personDto, Long personId);
}
