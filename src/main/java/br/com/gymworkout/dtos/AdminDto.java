package br.com.gymworkout.dtos;

import br.com.gymworkout.converters.entity.PersonVisitor;
import br.com.gymworkout.model.Person;

public class AdminDto extends PersonDto {

	@Override
	public Person convert(PersonVisitor personVisitor) {
		return personVisitor.visit(this);
	}
}
