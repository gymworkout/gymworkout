package br.com.gymworkout.dtos;

import br.com.gymworkout.config.tenant.TenantId;
import br.com.gymworkout.converters.entity.PersonVisitor;
import br.com.gymworkout.infra.Visitable;
import br.com.gymworkout.model.Person;
import br.com.gymworkout.model.PersonType;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = PROPERTY, property = "type", visible = true)
@JsonSubTypes({
		@JsonSubTypes.Type(value = TrainerDto.class, name = "TRAINER"),
		@JsonSubTypes.Type(value = AdminDto.class, name = "ADMIN"),
		@JsonSubTypes.Type(value = MemberDto.class, name = "MEMBER")
})
@Data
public abstract class PersonDto implements Visitable<Person, PersonVisitor> {
	private Long id;

	@NotEmpty(message = "{personDto.name}")
	private String name;
	@NotNull
	private String cpf;
	@NotNull
	private String password;

	private boolean enabled;
	private TenantId tenantId;

	@NotNull
	private PersonType type;
}
