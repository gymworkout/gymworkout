package br.com.gymworkout.dtos;

import br.com.gymworkout.converters.entity.PersonVisitor;
import br.com.gymworkout.model.Person;
import lombok.Data;

@Data
public class TrainerDto extends PersonDto {

	private String test;

	@Override
	public Person convert(PersonVisitor personVisitor) {
		return personVisitor.visit(this);
	}
}
