package br.com.gymworkout.infra;

import java.io.Serializable;

public class BeansLookupWrapper implements Serializable {

	public <T> T getBean(Class<T> beanClass) {
		return BeansLookup.getBean(beanClass);
	}
}
