package br.com.gymworkout.infra;

import lombok.extern.log4j.Log4j2;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityNotFoundException;
import java.util.Objects;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
@Log4j2
public class HandlerMechanism {

	@ExceptionHandler({EntityNotFoundException.class})
	public ResponseEntity notFound(Exception e) {
		JSONObject message = new JSONObject().put("message", e.getMessage());
		log.info(message, e);
		return ResponseEntity.status(NOT_FOUND).body(Objects.toString(message));
	}

	@ExceptionHandler({IllegalCallerException.class, IllegalArgumentException.class})
	public ResponseEntity badRequest(Exception e) {
		JSONObject message = new JSONObject().put("message", e.getMessage());
		log.info(message, e);
		return ResponseEntity.status(BAD_REQUEST).body(Objects.toString(message));
	}
}
