package br.com.gymworkout.infra;

public interface Visitable<D, V> {
	D convert(V v);
}
