package br.com.gymworkout.infra;

import br.com.gymworkout.config.tenant.TenantInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

@Configuration
public class WebConfig implements WebMvcConfigurer {

	@Autowired
	private TenantInterceptor tenantInterceptor;

	@Autowired
	private LocaleChangeInterceptor localeChangeInterceptor;

	@Override
	@DependsOn("InternationalizationConfig")
	public void addInterceptors(InterceptorRegistry registry) {
		localeChangeInterceptor.setParamName("language");

		registry.addInterceptor(localeChangeInterceptor);
		registry.addInterceptor(tenantInterceptor);
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**");
	}
}
