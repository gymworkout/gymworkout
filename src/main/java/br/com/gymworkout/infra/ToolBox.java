package br.com.gymworkout.infra;

import java.util.Optional;

public class ToolBox {

	private ToolBox() {
	}

	public static <T> T castIfIsInstanceOf(Class<T> clazz, Object o) {
		return Optional.of(o)
				.filter(clazz::isInstance)
				.map(clazz::cast).orElse(null);
	}
}
