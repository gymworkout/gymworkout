package br.com.gymworkout.infra;

public class Constants {
	private Constants() {
	}


	public static final String LOCATION = "Location";
	public static final String API_PATH = "/gym-service/";
	public static final String PERSON = "person";
	public static final String X_TENANT_ID = "X-TENANT-ID";
	public static final String CPF = "CPF";
	public static final String USER_ID = "USER_ID";

}
