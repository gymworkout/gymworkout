package br.com.gymworkout.infra;


import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component("lookup")
public class BeansLookup implements ApplicationContextAware {
	private static ApplicationContext context;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {

		if (Objects.isNull(context)) {
			context = applicationContext;
		}
	}

	static <T> T getBean(Class<T> beanClass) {
		return context.getBean(beanClass);
	}
}
