package br.com.gymworkout.infra;

import br.com.gymworkout.util.TestUtil;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.jupiter.api.Assertions.assertThrows;

class BeansLookupWrapperTest {

	private BeansLookupWrapper wrapper;
	private ApplicationContext ctx;

	@BeforeEach
	void setUp() {
		wrapper = new BeansLookupWrapper();
		ctx = TestUtil.simulateBeanInContext(new StringBuilder());
		TestUtil.register(ctx);
	}

	@Test
	void shouldGetBeanLoadedInApplicationContext() {
		Object obj = wrapper.getBean(StringBuilder.class);
		Assert.assertThat(obj, instanceOf(StringBuilder.class));
	}


	@Test
	void shouldThrowNoSuchBeanDefinitionExceptionWhenTryingToAssignNewApplicationContext() {
		ctx = TestUtil.simulateBeanInContext(new StringBuffer());
		TestUtil.register(ctx);
		assertThrows(NoSuchBeanDefinitionException.class, () -> wrapper.getBean(StringBuffer.class));
	}
}