package br.com.gymworkout.infra;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

class HandlerMechanismTest {

	private static final String EXCEPTION_BODY = "{\"message\":\"msg\"}";

	private HandlerMechanism handlerMechanism;

	@BeforeEach
	void setUp() {
		handlerMechanism = new HandlerMechanism();
	}

	@Test
	void shouldReturnNotFound() {
		ResponseEntity response = handlerMechanism.notFound(new Exception("msg"));
		assertEquals(NOT_FOUND, response.getStatusCode());
		assertEquals(EXCEPTION_BODY, response.getBody());
	}

	@Test
	void shouldReturnBadRequest() {
		ResponseEntity response = handlerMechanism.badRequest(new Exception("msg"));
		assertEquals(BAD_REQUEST, response.getStatusCode());
		assertEquals(EXCEPTION_BODY, response.getBody());
	}
}