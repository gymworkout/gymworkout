package br.com.gymworkout.infra;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import static org.hamcrest.Matchers.instanceOf;
import static org.mockito.MockitoAnnotations.initMocks;

class BeansFactoryTest {

	private BeansFactory beansFactory;

	@BeforeEach
	void setUp() {
		beansFactory = new BeansFactory();
		initMocks(this);
	}

	@Test
	void localeChangeInterceptor() {
		Assert.assertThat(beansFactory.localeChangeInterceptor(), instanceOf(LocaleChangeInterceptor.class));
	}
}