package br.com.gymworkout.infra;

import br.com.gymworkout.config.tenant.TenantInterceptor;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

class WebConfigTest {

	@Mock
	private InterceptorRegistry registry;

	@Mock
	private TenantInterceptor tenantInterceptor;

	@Spy
	private LocaleChangeInterceptor localeChangeInterceptor;

	@InjectMocks
	private WebConfig webConfig;

	@BeforeEach
	void setUp() {
		initMocks(this);
	}

	@Test
	void shouldRegistryNInterceptorsQuantity() {
		webConfig.addInterceptors(registry);
		verify(registry, times(2)).addInterceptor(any(HandlerInterceptorAdapter.class));
	}

	@Test
	void shouldSetCorrectLanguageParam() {
		webConfig.addInterceptors(registry);
		Assert.assertThat(localeChangeInterceptor.getParamName(), equalTo("language"));
	}
}