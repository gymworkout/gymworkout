package br.com.gymworkout.infra;

import br.com.gymworkout.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.MockitoAnnotations.initMocks;

class BeansLookupTest {

	private BeansLookup beansLookup;
	private ApplicationContext ctx;

	@BeforeEach
	void setUp() {
		initMocks(this);
		beansLookup = new BeansLookup();
		ctx = TestUtil.simulateBeanInContext(new StringBuilder());
		beansLookup.setApplicationContext(ctx);
	}

	@Test
	void shouldGetTheBeanLoadedAtApplicationStartup() {
		assertThat(BeansLookup.getBean(StringBuilder.class), instanceOf(StringBuilder.class));
	}

	@Test
	void shouldThrownExceptionWhenNewAttemptToManipulateApplicationContextOccurs() {
		ctx = TestUtil.simulateBeanInContext(new StringBuffer());
		beansLookup.setApplicationContext(ctx);
		assertThrows(NoSuchBeanDefinitionException.class, () -> BeansLookup.getBean(StringBuffer.class));
	}
}