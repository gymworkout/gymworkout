package br.com.gymworkout.infra;

import br.com.gymworkout.model.Member;
import br.com.gymworkout.model.Person;
import br.com.gymworkout.model.Trainer;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

class ToolBoxTest {

	@Test
	void shouldCastIfIsInstanceOf() {
		Person person = new Trainer();
		Trainer trainer = ToolBox.castIfIsInstanceOf(Trainer.class, person);
		assertThat(trainer, instanceOf(Trainer.class));

	}

	@Test
	void shouldNotCastIfNotIsInstanceOf() {
		Person person = new Trainer();
		Member member = ToolBox.castIfIsInstanceOf(Member.class, person);
		assertThat(member, nullValue());
	}
}