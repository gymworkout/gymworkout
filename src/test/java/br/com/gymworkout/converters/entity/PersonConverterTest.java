package br.com.gymworkout.converters.entity;

import br.com.gymworkout.config.tenant.TenantContext;
import br.com.gymworkout.config.tenant.TenantId;
import br.com.gymworkout.dtos.AdminDto;
import br.com.gymworkout.dtos.MemberDto;
import br.com.gymworkout.dtos.PersonDto;
import br.com.gymworkout.dtos.TrainerDto;
import br.com.gymworkout.model.Person;
import br.com.gymworkout.model.PersonType;
import br.com.gymworkout.model.Trainer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

class PersonConverterTest {

	private PersonConverter personConverter;

	@BeforeEach
	void setUp() {
		personConverter = new PersonConverter();
		TenantContext.setCurrentTenant("LIV");
	}

	@Test
	void visitTrainer() {

		TrainerDto trainerDto = new TrainerDto();
		trainerDto.setTest("test");

		setPersonDtoProps(trainerDto);

		Trainer person = (Trainer) personConverter.visit(trainerDto);

		assertThat(person.getId(), equalTo(1L));
		assertThat(person.getCpf(), notNullValue());
		assertThat(person.getName(), equalTo("zé"));
		assertThat(person.isEnabled(), equalTo(true));
		assertThat(person.getTenantId(), equalTo(TenantId.LIV));
		assertThat(person.getType(), equalTo(PersonType.ADMIN));
		assertThat(person.getTest(), equalTo("test"));
	}

	@Test
	void visitAdmin() {
		AdminDto adminDto = new AdminDto();

		setPersonDtoProps(adminDto);

		Person person = personConverter.visit(adminDto);

		assertThat(person.getId(), equalTo(1L));
		assertThat(person.getCpf(), notNullValue());
		assertThat(person.getName(), equalTo("zé"));
		assertThat(person.isEnabled(), equalTo(true));
		assertThat(person.getTenantId(), equalTo(TenantId.LIV));
		assertThat(person.getType(), equalTo(PersonType.ADMIN));
	}

	@Test
	void visitMember() {

		MemberDto memberDto = new MemberDto();

		setPersonDtoProps(memberDto);

		Person person = personConverter.visit(memberDto);

		assertThat(person.getId(), equalTo(1L));
		assertThat(person.getCpf(), notNullValue());
		assertThat(person.getName(), equalTo("zé"));
		assertThat(person.isEnabled(), equalTo(true));
		assertThat(person.getTenantId(), equalTo(TenantId.LIV));
		assertThat(person.getType(), equalTo(PersonType.ADMIN));
	}

	private void setPersonDtoProps(PersonDto personDto) {
		personDto.setId(1L);
		personDto.setPassword("any");
		personDto.setCpf("123");
		personDto.setName("zé");
		personDto.setEnabled(true);
		personDto.setType(PersonType.ADMIN);
	}
}