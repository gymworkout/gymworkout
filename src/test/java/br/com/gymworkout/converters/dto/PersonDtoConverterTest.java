package br.com.gymworkout.converters.dto;

import br.com.gymworkout.config.tenant.TenantId;
import br.com.gymworkout.dtos.AdminDto;
import br.com.gymworkout.dtos.MemberDto;
import br.com.gymworkout.dtos.TrainerDto;
import br.com.gymworkout.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

class PersonDtoConverterTest {

	private PersonDtoConverter personDtoConverter;

	@BeforeEach
	void setUp() {
		personDtoConverter = new PersonDtoConverter();
	}

	@Test
	void visitTrainerDto() {

		Trainer trainer = new Trainer();
		trainer.setTest("test");

		setPersonDtoProps(trainer);

		TrainerDto trainerDto = (TrainerDto) personDtoConverter.visit(trainer);

		assertThat(trainerDto.getId(), equalTo(1L));
		assertThat(trainerDto.getCpf(), notNullValue());
		assertThat(trainerDto.getName(), equalTo("zé"));
		assertThat(trainerDto.isEnabled(), equalTo(true));
		assertThat(trainerDto.getTenantId(), equalTo(TenantId.LIV));
		assertThat(trainerDto.getType(), equalTo(PersonType.ADMIN));
		assertThat(trainerDto.getTest(), equalTo("test"));
	}

	@Test
	void visitAdmin() {
		Admin admin = new Admin();

		setPersonDtoProps(admin);

		AdminDto adminDto = (AdminDto) personDtoConverter.visit(admin);

		assertThat(adminDto.getId(), equalTo(1L));
		assertThat(adminDto.getCpf(), notNullValue());
		assertThat(adminDto.getName(), equalTo("zé"));
		assertThat(adminDto.isEnabled(), equalTo(true));
		assertThat(adminDto.getTenantId(), equalTo(TenantId.LIV));
		assertThat(adminDto.getType(), equalTo(PersonType.ADMIN));
	}

	@Test
	void visitMember() {

		Member member = new Member();

		setPersonDtoProps(member);

		MemberDto memberDto = (MemberDto) personDtoConverter.visit(member);

		assertThat(memberDto.getId(), equalTo(1L));
		assertThat(memberDto.getCpf(), notNullValue());
		assertThat(memberDto.getName(), equalTo("zé"));
		assertThat(memberDto.isEnabled(), equalTo(true));
		assertThat(memberDto.getTenantId(), equalTo(TenantId.LIV));
		assertThat(memberDto.getType(), equalTo(PersonType.ADMIN));
	}

	private void setPersonDtoProps(Person person) {
		person.setId(1L);
		person.setPassword("any");
		person.setCpf("123");
		person.setName("zé");
		person.setEnabled(true);
		person.setType(PersonType.ADMIN);
		person.setTenantId(TenantId.LIV);
	}
}