package br.com.gymworkout.services;

import br.com.gymworkout.dtos.AdminDto;
import br.com.gymworkout.dtos.MemberDto;
import br.com.gymworkout.dtos.PersonDto;
import br.com.gymworkout.model.Admin;
import br.com.gymworkout.model.Member;
import br.com.gymworkout.model.Person;
import br.com.gymworkout.repositories.PersonRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class PersonServiceImplTest {

	@Mock
	private PersonRepository personRepository;

	@InjectMocks
	private PersonServiceImpl personService;

	@BeforeEach
	void setUp() {
		initMocks(this);
	}

	@Test
	void getSuccess() {
		Person person = new Member();
		person.setId(1L);

		when(personRepository.findById(anyLong())).thenReturn(Optional.of(person));

		PersonDto personDto = personService.get(1L);

		assertThat(personDto, notNullValue());
		assertThat(personDto.getId(), equalTo(1L));
		assertThat(personDto.getPassword(), equalTo("***"));
	}

	@Test
	void getNotFound() {
		when(personRepository.findById(anyLong())).thenReturn(Optional.empty());
		assertThrows(EntityNotFoundException.class, () -> personService.get(1L));
	}

	@Test
	void create() {
		Person person = new Admin();
		person.setId(1L);

		when(personRepository.save(any(Person.class))).thenReturn(person);

		PersonDto personDto = new AdminDto();
		personDto.setPassword("any");

		personDto = personService.create(personDto);

		assertThat(personDto, notNullValue());
		assertThat(personDto.getId(), equalTo(1L));
		assertThat(personDto.getPassword(), equalTo("***"));
	}


	@Test
	void updateSuccess() {
		Person person = new Admin();
		person.setId(1L);
		person.setPassword("123");

		when(personRepository.existsById(anyLong())).thenReturn(true);
		when(personRepository.save(any(Person.class))).thenReturn(person);

		AdminDto personDto = new AdminDto();
		personDto.setId(1L);
		personDto.setPassword("123");

		PersonDto updatedDto = personService.update(personDto, 1L);

		assertThat(updatedDto.getId(), equalTo(1l));
		assertThat(updatedDto.getPassword(), equalTo("***"));
	}

	@Test
	void updateNotFound() {
		when(personRepository.existsById(anyLong())).thenReturn(false);
		assertThrows(EntityNotFoundException.class, () -> personService.update(new MemberDto(), 1L));
	}
}