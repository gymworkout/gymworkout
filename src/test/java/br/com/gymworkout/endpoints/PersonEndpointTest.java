package br.com.gymworkout.endpoints;

import br.com.gymworkout.dtos.AdminDto;
import br.com.gymworkout.dtos.PersonDto;
import br.com.gymworkout.dtos.TrainerDto;
import br.com.gymworkout.services.PersonService;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class PersonEndpointTest {

	@InjectMocks
	private PersonEndpoint personEndpoint;

	@Mock
	private PersonService personService;

	@BeforeEach
	void setUp() {
		initMocks(this);
	}

	@Test
	void save() {
		PersonDto personDto = new TrainerDto();
		personDto.setId(1L);

		when(personService.create(any(PersonDto.class))).thenReturn(personDto);

		ResponseEntity response = personEndpoint.save(personDto);

		assertThat(response.getHeaders().get("Location"), equalTo(Arrays.asList("/gym-service/person/1")));
	}

	@Test
	void get() {


		PersonDto personDto = new AdminDto();
		personDto.setId(1L);
		personDto.setName("crebis");

		when(personService.get(anyLong())).thenReturn(personDto);

		ResponseEntity response = personEndpoint.get(1L);

		JSONObject person = new JSONObject(response.getBody());

		assertThat(person.get("id"), equalTo(1L));
		assertThat(person.get("name"), equalTo("crebis"));
		assertThrows(JSONException.class, () -> person.get("cpf"));
	}

	@Test
	void update() {
		PersonDto personDto = new AdminDto();
		personDto.setId(1L);
		personDto.setName("crebis");

		when(personService.update(any(PersonDto.class),anyLong())).thenReturn(personDto);

		ResponseEntity response = personEndpoint.update(1L, new AdminDto());

		JSONObject person = new JSONObject(response.getBody());

		assertThat(person.get("id"), equalTo(1L));
		assertThat(person.get("name"), equalTo("crebis"));
		assertThrows(JSONException.class, () -> person.get("cpf"));
	}
}