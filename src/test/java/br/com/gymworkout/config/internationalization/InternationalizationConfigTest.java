package br.com.gymworkout.config.internationalization;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

class InternationalizationConfigTest {


	private InternationalizationConfig config;


	@BeforeEach
	void setUp() {
		config = new InternationalizationConfig();
		initMocks(this);
	}

	@Test
	void getValidator() {
		LocalValidatorFactoryBean validator = config.getValidator();
		assertThat(validator, instanceOf(LocalValidatorFactoryBean.class));
	}

	@Test
	void messageSource() {

		MessageSource messageSource = config.messageSource();
		assertThat(messageSource, instanceOf(ReloadableResourceBundleMessageSource.class));

		ReloadableResourceBundleMessageSource resource = (ReloadableResourceBundleMessageSource) config.messageSource();
		assertThat(resource.getBasenameSet().iterator().next(), equalTo("classpath:messages"));
	}

	@Test
	void defaultLanguageShouldBeBr() {
		LocaleResolver localeResolver = config.localeResolver();
		assertThat(localeResolver, instanceOf(SessionLocaleResolver.class));

		SessionLocaleResolver sessionLocaleResolver = (SessionLocaleResolver) localeResolver;
		assertThat(sessionLocaleResolver.resolveLocale(new MockHttpServletRequest()).getLanguage(), equalTo("br"));
	}
}