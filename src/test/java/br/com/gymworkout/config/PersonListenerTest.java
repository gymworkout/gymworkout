package br.com.gymworkout.config;

import br.com.gymworkout.config.tenant.TenantId;
import br.com.gymworkout.model.Authority;
import br.com.gymworkout.model.Member;
import br.com.gymworkout.model.Person;
import br.com.gymworkout.model.User;
import br.com.gymworkout.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.ApplicationContext;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class PersonListenerTest {

	@Mock
	private ApplicationContext ctx;

	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private PersonListener personListener;

	@Captor
	private ArgumentCaptor<User> captor;

	private Person person;

	@BeforeEach
	void setUp() {
		person = getPersonMock();
		initMocks(this);
	}

	@Test
	void onPostPersist() {
		when(ctx.getBean(any(Class.class))).thenReturn(userRepository);
		personListener.onPostPersist(person);
		verify(userRepository).save(captor.capture());
		assertEqualityBetweenUserAndPerson(captor.getValue());
		assertNull(captor.getValue().getId());
	}

	@Test
	void onPostUpdate() {
		when(ctx.getBean(any(Class.class))).thenReturn(userRepository);
		personListener.onPostUpdate(person);
		verify(userRepository).save(captor.capture());
		assertEqualityBetweenUserAndPerson(captor.getValue());
		assertThat(captor.getValue().getId(), equalTo(person.getId()));
	}

	private void assertEqualityBetweenUserAndPerson(User user) {
		assertThat(user.getUsername(), equalTo(person.getCpf()));
		assertThat(user.getPassword(), equalTo(person.getPassword()));
		assertThat(user.getCpf(), equalTo(person.getCpf()));
		assertThat(user.getAuthority(), equalTo(person.getAuthority()));
		assertThat(user.getTenantId(), equalTo(person.getTenantId()));
		assertThat(user.isEnabled(), equalTo(person.isEnabled()));
	}

	private Person getPersonMock() {
		Person person = new Member();
		person.setId(1L);
		person.setAuthority(Authority.ROLE_MEMBER);
		person.setCpf("123");
		person.setEnabled(true);
		person.setTenantId(TenantId.LIV);
		person.setPassword("321");
		return person;
	}
}