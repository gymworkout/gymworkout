package br.com.gymworkout.config.datasource;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.test.util.ReflectionTestUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class DataSourceConfigTest {

	@Mock
	private Properties props;

	@Captor
	private ArgumentCaptor<String> captor;

	@InjectMocks
	private DataSourceConfig dataSourceConfig;

	@BeforeEach
	void setUp() throws IOException {
		initMocks(this);
		doNothing().when(props).load(Mockito.any(InputStream.class));
		ReflectionTestUtils.setField(dataSourceConfig, "file", "file");
	}

	@Test
	void dev() throws IOException {
		doTest(dataSourceConfig.dev());
	}

	@Test
	void prod() throws IOException {
		doTest(dataSourceConfig.prod());
	}

	private void doTest(DataSource dataSource) {
		assertThat(dataSource, instanceOf(DriverManagerDataSource.class));
		DriverManagerDataSource driverManagerDataSource = (DriverManagerDataSource) dataSource;

		verify(props, times(2)).get(captor.capture());
		assertThat(captor.getAllValues().get(0), is("hibernate.connection.url"));
		assertThat(captor.getAllValues().get(1), is("hibernate.connection.username"));
		assertThat(driverManagerDataSource.getConnectionProperties(), notNullValue());
	}
}