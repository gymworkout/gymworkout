package br.com.gymworkout.config.security;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.userdetails.DaoAuthenticationConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class SecurityConfigurationTest {

	@InjectMocks
	private SecurityConfiguration config;

	@Mock
	private UserService userService;

	@Mock
	private DaoAuthenticationConfigurer<AuthenticationManagerBuilder, UserDetailsService> configurer;

	@Mock
	private ObjectPostProcessor<Object> objectPostProcessor;

	@BeforeEach
	void setUp() {
		initMocks(this);
	}

	@Test
	void configureAuthenticationBuilderAccordingly() throws Exception {
		AuthenticationManagerBuilder spy = spy(new AuthenticationManagerBuilder(objectPostProcessor));

		doReturn(configurer).when(spy).userDetailsService(userService);
		doReturn(null).when(configurer).passwordEncoder(any(PasswordEncoder.class));

		config.configure(spy);

		verify(spy).userDetailsService(userService);
		verify(spy.userDetailsService(userService)).passwordEncoder(any(PasswordEncoder.class));
	}


	@Test
	void passwordEncoder() {
		assertThat(config.passwordEncoder(), instanceOf(BCryptPasswordEncoder.class));
	}
}