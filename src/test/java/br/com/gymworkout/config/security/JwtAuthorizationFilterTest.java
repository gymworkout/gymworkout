package br.com.gymworkout.config.security;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import java.io.IOException;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

class JwtAuthorizationFilterTest {

	public static final String VALID_TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9." +
			"eyJpc3MiOiJzZWN1cmUtYXBpIiwiYXVkIjoic2VjdXJlLWFwcCIsInN1YiI6IjM0MTE0MCIsImV4cCI6OTAwMDAwMTU1NTY4MjgxNiwicm9sIjpbIlJPTEVfQURNSU4iXX0." +
			"XuLHqqTm49wyN6w1s7LeqaKEt4x6iREV0-l2aeR0DjKPOCDCTtj5GoSHznB8Ed8FYRW-XTU3rmtDP5uXb9IAMw";
	@Mock
	private AuthenticationManager authenticationManager;

	@InjectMocks
	private JwtAuthorizationFilter authorizationFilter;

	@Spy
	private FilterChain filterChainSpy;

	@BeforeEach
	void setUp() {
		initMocks(this);
		authorizationFilter = new JwtAuthorizationFilter(authenticationManager);
		SecurityContextHolder.getContext().setAuthentication(null);
	}

	@ParameterizedTest
	@ValueSource(strings = {"", "not_starts_with_Bearer"})
	void shouldThrowIllegalArgumentExceptionWhenAuthorizationIsMalformed(String value) throws IOException, ServletException {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addHeader("Authorization", value);

		authorizationFilter.doFilterInternal(request, new MockHttpServletResponse(), new MockFilterChain());

		assertNull(SecurityContextHolder.getContext().getAuthentication());
	}

	@Test
	void shouldThrowIllegalArgumentWhenAuthorizationHeaderIsNotProvided() throws IOException, ServletException {
		MockHttpServletRequest request = new MockHttpServletRequest();

		authorizationFilter.doFilterInternal(request, new MockHttpServletResponse(), new MockFilterChain());

		assertNull(SecurityContextHolder.getContext().getAuthentication());
	}
	@Test
	void shouldAuthorizeSuccessfully() throws IOException, ServletException {
		MockHttpServletRequest request = new MockHttpServletRequest();
		MockHttpServletResponse response = new MockHttpServletResponse();
		request.addHeader("Authorization", VALID_TOKEN);

		authorizationFilter.doFilterInternal(request, response, filterChainSpy);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		verify(filterChainSpy).doFilter(request, response);
		assertThat(authentication.getAuthorities(), equalTo(asList(new SimpleGrantedAuthority("ROLE_ADMIN"))));
		assertThat(authentication.getPrincipal(), equalTo("341140"));
		assertThat(authentication.getCredentials(), nullValue());
	}


}