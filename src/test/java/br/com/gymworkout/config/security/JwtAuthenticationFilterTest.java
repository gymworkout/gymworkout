package br.com.gymworkout.config.security;

import br.com.gymworkout.config.tenant.TenantId;
import br.com.gymworkout.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

class JwtAuthenticationFilterTest {

	@InjectMocks
	private JwtAuthenticationFilter filter;

	@Mock
	private AuthenticationManager manager;

	@Captor
	private ArgumentCaptor<UsernamePasswordAuthenticationToken> captor;

	@BeforeEach
	void setUp() {
		initMocks(this);
	}

	@Test
	void attemptAuthentication() {

		MockHttpServletRequest request = getMockHttpServletRequest();

		filter.attemptAuthentication(request, new MockHttpServletResponse());

		verify(manager).authenticate(captor.capture());

		UsernamePasswordAuthenticationToken auth = captor.getValue();
		assertThat(auth.getPrincipal(), equalTo(request.getParameter("cpf")));
		assertThat(auth.getCredentials(), equalTo(request.getParameter("password")));
	}

	@Test
	void successfulAuthentication() {

		MockHttpServletResponse response = new MockHttpServletResponse();
		User user = new User();
		user.setId(1L);
		user.setCpf("123");
		user.setTenantId(TenantId.LIV);
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user, "321");

		filter.successfulAuthentication(null, response, null, auth);

		assertThat(response.getHeader(SecurityConstants.TOKEN_HEADER), startsWith("Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9"));
	}

	private MockHttpServletRequest getMockHttpServletRequest() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.setParameter("cpf", "123");
		request.setParameter("password", "321");
		return request;
	}
}