package br.com.gymworkout.config.security;

import br.com.gymworkout.model.User;
import br.com.gymworkout.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class UserServiceTest {

	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private UserService userService;

	@BeforeEach
	void setUp() {
		initMocks(this);
	}

	@Test
	void shouldRetrieveUserWithSuccess() {
		when(userRepository.findByCpf(anyString())).thenReturn(Optional.of(new User()));
		UserDetails userDetails = userService.loadUserByUsername(anyString());
		assertThat(userDetails, instanceOf(UserDetails.class));
		assertThat(userDetails, instanceOf(User.class));
	}

	@Test
	void shouldThrowUserNotFoundException() {
		when(userRepository.findByCpf(anyString())).thenReturn(Optional.empty());
		assertThrows(UsernameNotFoundException.class, () -> userService.loadUserByUsername(anyString()));
	}
}