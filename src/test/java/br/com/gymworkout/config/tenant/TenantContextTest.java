package br.com.gymworkout.config.tenant;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class TenantContextTest {

	@BeforeEach
	void setUp() {
		TenantContext.setCurrentTenant("LIV");
	}

	@Test
	void shouldGetCurrentTenant() {
		assertEquals("LIV", TenantContext.getCurrentTenant());
	}

	@Test
	void shouldCleanUp() {
		TenantContext.clear();
		assertNull(TenantContext.getCurrentTenant());
	}

	@Test
	void shouldGetCurrentTenantEnum() {
		Assert.assertThat(TenantContext.getCurrentTenantEnum(), equalTo(TenantId.LIV));
	}
}