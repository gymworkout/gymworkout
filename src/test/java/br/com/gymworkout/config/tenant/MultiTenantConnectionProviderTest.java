package br.com.gymworkout.config.tenant;

import br.com.gymworkout.infra.BeansLookupWrapper;
import org.hibernate.engine.jdbc.connections.internal.DriverManagerConnectionProviderImpl;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.Properties;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class MultiTenantConnectionProviderTest {

	@Mock
	private BeansLookupWrapper lookWrapper;

	@Mock
	private DriverManagerDataSource dataSource;

	@Mock
	private Properties properties;

	@Mock
	private DriverManagerConnectionProviderImpl connectionProvider;

	@Captor
	private ArgumentCaptor<Class<DriverManagerDataSource>> dataSourceCaptor;

	@InjectMocks
	private MultiTenantConnectionProvider provider;

	@BeforeEach
	void setUp() {
		initMocks(this);
		when(lookWrapper.getBean(ArgumentMatchers.<Class<DriverManagerDataSource>>any())).thenReturn(dataSource);
		when(dataSource.getConnectionProperties()).thenReturn(properties);
		doNothing().when(connectionProvider).configure(properties);
		provider.start();
	}

	@Test
	void getAnyConnectionProvider() {
		assertThat(provider.getAnyConnectionProvider(), instanceOf(ConnectionProvider.class));

	}

	@Test
	void selectConnectionProvider() {
		assertThat(provider.selectConnectionProvider("tenantId"), instanceOf(ConnectionProvider.class));
	}

	@Test
	void start() {
		provider.start();
		verify(lookWrapper, Mockito.times(2)).getBean(dataSourceCaptor.capture());
		assertThat(dataSourceCaptor.getValue().getSimpleName(), is("DriverManagerDataSource"));
	}
}

