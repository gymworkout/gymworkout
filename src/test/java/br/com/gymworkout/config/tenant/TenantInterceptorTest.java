package br.com.gymworkout.config.tenant;

import br.com.gymworkout.model.User;
import br.com.gymworkout.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static br.com.gymworkout.config.tenant.TenantId.LIV;
import static br.com.gymworkout.infra.Constants.CPF;
import static br.com.gymworkout.infra.Constants.X_TENANT_ID;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class TenantInterceptorTest {

	@InjectMocks
	private TenantInterceptor interceptor;

	@Mock
	private UserRepository userRepository;

	@BeforeEach
	void setUp() {
		initMocks(this);
		TenantContext.clear();
	}

	@Test
	void shouldThrowIllegalCallerExceptionWhenProvidedTenantIdIsNotInPredefinedList() {
		assertThrows(IllegalCallerException.class, () -> interceptor.preHandle(mockHttpServletRequest("undefined"), new MockHttpServletResponse(), new Object()));
		assertThrows(IllegalCallerException.class, () -> interceptor.preHandle(mockHttpServletRequest(""), new MockHttpServletResponse(), new Object()));
		assertThrows(IllegalCallerException.class, () -> interceptor.preHandle(new MockHttpServletRequest(), new MockHttpServletResponse(), new Object()));
		assertNull(TenantContext.getCurrentTenant());
	}

	@Test
	void shouldReturnTrueAndSetCorrectContextConfigWhenProvidedTenantIsInPredefinedList() {
		boolean isEverythingOk = interceptor.preHandle(mockHttpServletRequest(LIV.name()), new MockHttpServletResponse(), new Object());
		assertTrue(isEverythingOk);
		assertEquals(LIV.name(), TenantContext.getCurrentTenant());
	}

	@Test
	void shouldCleanUpContext() {
		TenantContext.setCurrentTenant("anyone");
		interceptor.postHandle(new MockHttpServletRequest(), new MockHttpServletResponse(), new Object(), new ModelAndView());
		assertNull(TenantContext.getCurrentTenant());
	}

	@Test
	void shouldDiscoveryCurrentTenant() {
		User user = new User();
		user.setId(1L);
		user.setTenantId(TenantId.HABITUS);
		user.setCpf("34114035869");

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addHeader(CPF, "34114035869");

		when(userRepository.findByCpf(anyString())).thenReturn(Optional.of(user));
		interceptor.preHandle(request, new MockHttpServletResponse(), new Object());

		assertThat(TenantContext.getCurrentTenant(), equalTo("HABITUS"));
	}

	@Test
	void shouldTenantIdHeaderHasPreferenceOverDiscoveryByCpf() {
		User user = new User();
		user.setId(1L);
		user.setTenantId(TenantId.HABITUS);
		user.setCpf("341");

		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addHeader(CPF, "341");
		request.addHeader(X_TENANT_ID, LIV.name());

		when(userRepository.findByCpf(anyString())).thenReturn(Optional.of(user));
		interceptor.preHandle(request, new MockHttpServletResponse(), new Object());

		assertThat(TenantContext.getCurrentTenant(), equalTo(LIV.name()));
		verify(userRepository, times(0)).findByCpf(anyString());
	}

	private HttpServletRequest mockHttpServletRequest(String tenantId) {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addHeader(X_TENANT_ID, tenantId);
		return request;
	}
}