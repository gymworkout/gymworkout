package br.com.gymworkout.config.tenant;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


class TenantIdentifierResolverTest {

	private TenantIdentifierResolver tenantIdentifierResolver;

	@BeforeEach
	void setUp() {
		tenantIdentifierResolver = new TenantIdentifierResolver();
		TenantContext.clear();
	}

	@Test
	void ShouldReturnDefaultTenantWhenContextIsEmpty() {
		assertEquals("default", tenantIdentifierResolver.resolveCurrentTenantIdentifier());
	}

	@Test
	void ShouldReturnTenantContextValue() {
		TenantContext.setCurrentTenant("client1");
		assertEquals("client1", tenantIdentifierResolver.resolveCurrentTenantIdentifier());

		TenantContext.setCurrentTenant("client2");
		assertEquals("client2", tenantIdentifierResolver.resolveCurrentTenantIdentifier());

	}

	@Test
	void shouldReturnTrueOnValidateExistingCurrentSessions() {
		assertTrue(tenantIdentifierResolver.validateExistingCurrentSessions());
	}
}