package br.com.gymworkout.util;

import br.com.gymworkout.infra.BeansLookup;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

public class TestUtil {
	public TestUtil() {
	}

	public static ApplicationContext simulateBeanInContext(Object obj) {
		ConfigurableApplicationContext ctx = new GenericApplicationContext();
		ctx.getBeanFactory().registerSingleton(obj.getClass().getCanonicalName(), obj);
		ctx.refresh();
		return ctx;
	}

	public static void register(ApplicationContext ctx) {
		new BeansLookup().setApplicationContext(ctx);
	}
}